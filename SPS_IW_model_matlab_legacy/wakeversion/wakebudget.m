%% Initializzation

clear all
close all

%% Load wake data

opticflag = input('Type 0 for Q20, Type 1 for Q26 [0]: ');
if isempty(opticflag)
    opticflag = 0;
end
if opticflag==0
    opt='_lowGT';
else opt='';
    end
    

BPHd=dlmread(strcat('WakesBPHforHDTLnewbeta',opt,'.dat'),'',0,0);
BPVd=dlmread(strcat('WakesBPVforHDTLnewbeta',opt,'.dat'),'',0,0);
EFd=dlmread(strcat('WakesenamelFlangeforHDTLbeta',opt,'.dat'),'',0,0);
RF200d=dlmread(strcat('WakesRFcav200forHDTLbeta',opt,'.dat'),'',0,0);
RF800d=dlmread(strcat('WakesRFcav800forHDTLbeta',opt,'.dat'),'',0,0);
kickerd=dlmread(strcat('kickers2015_beta',opt,'.txt'),'',0,0);
walld=dlmread(strcat('wall_beta',opt,'.txt'),'',0,0);
transitionsd=dlmread(strcat('transitionsnew_beta',opt,'.txt'),'',0,0);

BPHf=real(BPHd(:,1));
BPHxdip=(BPHd(:,2));
BPHydip=(BPHd(:,3));
BPHxquad=(BPHd(:,4));
BPHyquad=(BPHd(:,5));
BPHX=BPHxdip+BPHxquad;
BPHY=BPHydip+BPHyquad;
BPH=[BPHf BPHxdip BPHydip BPHxquad BPHyquad];

BPVf=real(BPVd(:,1));
BPVxdip=(BPVd(:,2));
BPVydip=(BPVd(:,3));
BPVxquad=(BPVd(:,4));
BPVyquad=(BPVd(:,5));
BPVX=BPVxdip+BPVxquad;
BPVY=BPVydip+BPVyquad;
BPV=[BPVf BPVxdip BPVydip BPVxquad BPVyquad];

EFf=real(EFd(:,1));
EFxdip=(EFd(:,2));
EFydip=(EFd(:,3));
EFxquad=(EFd(:,4));
EFyquad=(EFd(:,5));
EFX=EFxdip+EFxquad;
EFY=EFydip+EFyquad;
EF=[EFf EFxdip EFydip EFxquad EFyquad];

RF200f=real(RF200d(:,1));
RF200xdip=(RF200d(:,2));
RF200ydip=(RF200d(:,3));
RF200xquad=(RF200d(:,4));
RF200yquad=(RF200d(:,5));
RF200X=RF200xdip+RF200xquad;
RF200Y=RF200ydip+RF200yquad;
RF200=[RF200f RF200xdip RF200ydip RF200xquad RF200yquad];

RF800f=real(RF800d(:,1));
RF800xdip=(RF800d(:,2));
RF800ydip=(RF800d(:,3));
RF800xquad=(RF800d(:,4));
RF800yquad=(RF800d(:,5));
RF800X=RF800xdip+RF800xquad;
RF800Y=RF800ydip+RF800yquad;
RF800=[RF800f RF800xdip RF800ydip RF800xquad RF800yquad];

kickerf=real(kickerd(:,1));
kickerxdip=(kickerd(:,2));
kickerydip=(kickerd(:,3));
kickerxquad=(kickerd(:,4));
kickeryquad=(kickerd(:,5));
kickerX=kickerxdip+kickerxquad;
kickerY=kickerydip+kickeryquad;
kicker=[kickerf kickerxdip kickerydip kickerxquad kickeryquad];

wallf=real(walld(:,1))*1e9;
wallxdip=(walld(:,2))/1000*1.00;
wallydip=(walld(:,4))/1000*1.00;
wallxquad=(walld(:,3))/1000*1.00;
wallyquad=(walld(:,5))/1000*1.00;
wallX=wallxdip+wallxquad;
wallY=wallydip+wallyquad;
wall=[wallf wallxdip wallydip wallxquad wallyquad];

transitionsf=real(transitionsd(:,1));
transitionsxdip=(transitionsd(:,2));
transitionsydip=(transitionsd(:,3));
transitionsxquad=(transitionsd(:,4));
transitionsyquad=(transitionsd(:,5));
transitionsX=transitionsxdip+transitionsxquad;
transitionsY=transitionsydip+transitionsyquad;
transitions=[transitionsf transitionsxdip transitionsydip transitionsxquad transitionsyquad];

%interpolazione
f=[1e-2:1e-2:10];
RF200xdipt=interp1(RF200f(1:2000),RF200xdip(1:2000),f);
RF800xdipt=interp1(RF800f,RF800xdip,f);
EFxdipt=interp1(EFf,EFxdip,f);
BPVxdipt=interp1(BPVf,BPVxdip,f);
%BPHxdipt=interp1(BPHf(1:10920),BPHxdip(1:10920),f);
BPHxdipt=interp1(BPHf,BPHxdip,f);
kickerxdipt=interp1(kickerf,kickerxdip,f);
transitionsxdipt=interp1(transitionsf,transitionsxdip,f);
wallxdipt=interp1(wallf,wallxdip,f);

RF200xquadt=interp1(RF200f(1:2000),RF200xquad(1:2000),f);
RF800xquadt=interp1(RF800f,RF800xquad,f);
EFxquadt=interp1(EFf,EFxquad,f);
BPVxquadt=interp1(BPVf,BPVxquad,f);
BPHxquadt=interp1(BPHf,BPHxquad,f);
kickerxquadt=interp1(kickerf,kickerxquad,f);
transitionsxquadt=interp1(transitionsf,transitionsxquad,f);
wallxquadt=interp1(wallf,wallxquad,f);


RF200ydipt=interp1(RF200f(1:2000),RF200ydip(1:2000),f);
RF800ydipt=interp1(RF800f,RF800ydip,f);
EFydipt=interp1(EFf,EFydip,f);
BPVydipt=interp1(BPVf,BPVydip,f);
BPHydipt=interp1(BPHf,BPHydip,f);
kickerydipt=interp1(kickerf,kickerydip,f);
transitionsydipt=interp1(transitionsf,transitionsydip,f);
wallydipt=interp1(wallf,wallydip,f);

RF200yquadt=interp1(RF200f(1:2000),RF200yquad(1:2000),f);
RF800yquadt=interp1(RF800f,RF800yquad,f);
EFyquadt=interp1(EFf,EFyquad,f);
BPVyquadt=interp1(BPVf,BPVyquad,f);
BPHyquadt=interp1(BPHf,BPHyquad,f);
kickeryquadt=interp1(kickerf,kickeryquad,f);
transitionsyquadt=interp1(transitionsf,transitionsyquad,f);
wallyquadt=interp1(wallf,wallyquad,f);

%% Building of the wake model

disp=0.04;
%KTXim=dlmread('ZimTx_Q20.txt','',0,0);

loadflag = input('Type 0 for the full SPS model, Type 1 for interactive building of the model [0]: ');
if isempty(loadflag)
    loadflag = 0;
end

if loadflag==0
    a1=1; s1= '_EF';
    a2=1; s2='_BPH';
    a3=1; s3='_BPV';
    a4=1; s4='_RF200';
    a5=1; s5='_RF800';
    a6=1; s6='_kicker';
    a7=1; s7='_wall';
    a8=1; s8='_steps';
    a=[a1 a2 a3 a3 a5 a6 a7 a8];
    s=[s1 s2 s3 s4 s5 s6 s7 s8 opt];
end

if loadflag==1
    a1=input('Do you want include enamel flanges (y/n) [y]: ','s');
    if isempty(a1)
    a1 = 'y';
    end
   if strcmp(a1,'y')
       a1=1;
       s1='_EF';
   else a1=0;
       s1='';
   end
   a2=input('Do you want include BPH (y/n) [y]: ','s');
    if isempty(a2)
    a2 = 'y';
    end
   if strcmp(a2,'y')
       a2=1;
       s2='_BPH';
   else a2=0;
       s2='';
   end
   a3=input('Do you want include BPV (y/n) [y]: ','s');
    if isempty(a3)
    a3 = 'y';
    end
   if strcmp(a3,'y')
       a3=1;
       s3='_BPV';
   else a3=0;
       s3='';
   end
   a4=input('Do you want include RF200MHz cavities (y/n) [y]: ','s');
    if isempty(a4)
    a4 = 'y';
    end
   if strcmp(a4,'y')
       a4=1;
       s4='_RF200';
   else a4=0;
       s4='';
   end
   a5=input('Do you want include RF800 MHz cavities (y/n) [y]: ','s');
    if isempty(a5)
    a5 = 'y';
    end
   if strcmp(a5,'y')
       a5=1;
       s5='_RF800';
   else a5=0;
       s5='';
   end
    a6=input('Do you want include kickers (y/n) [y]: ','s');
    if isempty(a6)
    a6 = 'y';
    end
   if strcmp(a6,'y')
       a6=1;
       s6='_kicker';
   else a6=0;
       s6='';
   end
    a7=input('Do you want include wall (y/n) [y]: ','s');
    if isempty(a7)
    a7 = 'y';
    end
   if strcmp(a7,'y')
       a7=1;
       s7='_wall';
   else a7=0;
       s7='';
   end
   a8=input('Do you want include steps (y/n) [y]: ','s');
    if isempty(a8)
    a8 = 'y';
    end
   if strcmp(a8,'y')
       a8=1;
       s8='_steps';
   else a8=0;
       s8='';
   end
   a=[a1 a2 a3 a4 a5 a6 a7 a8];
   s=[s1 s2 s3 s4 s5 s6 s7 s8 opt];
end

%Somma

WXdipf=EFxdipt*a(1)+BPHxdipt*a(2)+BPVxdipt*a(3)+RF200xdipt*a(4)+RF800xdipt*a(5)+kickerxdipt*a(6)+wallxdipt*a(7)+transitionsxdipt*a(8);
WYdipf=EFydipt*a(1)+BPHydipt*a(2)+BPVydipt*a(3)+RF200ydipt*a(4)+RF800ydipt*a(5)+kickerydipt*a(6)+wallydipt*a(7)+transitionsydipt*a(8);
WXquadf=EFxquadt*a(1)+BPHxquadt*a(2)+BPVxquadt*a(3)+RF200xquadt*a(4)+RF800xquadt*a(5)+kickerxquadt*a(6)+wallxquadt*a(7)+transitionsxquadt*a(8);
WYquadf=EFyquadt*a(1)+BPHyquadt*a(2)+BPVyquadt*a(3)+RF200yquadt*a(4)+RF800yquadt*a(5)+kickeryquadt*a(6)+wallyquadt*a(7)+transitionsyquadt*a(8);

WdataSPSf=[f' WXdipf' WYdipf' WXquadf' WYquadf'];
save(strcat('WakemodelSPS',s),'WdataSPSf');

figure(1)
set(gca,'FontSize',20)
plot(f,WXdipf,'g','LineWidth',3);
hold on;
plot(f,WXquadf,'g--','LineWidth',3);
hold on;
grid on;
title('Horizontal SPS wake model')
xlabel('time [ns]')
ylabel('Wake [V/(fC)]')
legend('Driving','Detuning');
saveas(figure(1),strcat('XWakemodelSPS',s),'png')

figure(2)
set(gca,'FontSize',20)
plot(f,WYdipf,'g','LineWidth',3);
hold on;
plot(f,WYquadf,'g--','LineWidth',3);
hold on;
grid on;
title('Vertical SPS wake model')
xlabel('time [ns]')
ylabel('Wake [V/(fC)]')
legend('Driving','Detuning');
saveas(figure(1),strcat('YWakemodelSPS',s),'png')
