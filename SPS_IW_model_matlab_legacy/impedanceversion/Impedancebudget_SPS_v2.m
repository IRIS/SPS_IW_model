%Interactive building of the SPS impedance model for Q20 or Q26 optics
%The program computes also effective impedances, tune shifts and headtail growth rates
%% Initializzation
 
clear all
close all
cd('/media/HDD/Work/CERN/SPS_IW_model/SPSimp')  %home directory

%% Load impedance data

opticflag = input('Type 0 for Q20, Type 1 for Q26 [0]: ');
if isempty(opticflag)
    opticflag = 0;
end
if opticflag==0
    opt='_lowGT';
else opt='';
    
end

energyflag = input('Type 0 for 26GeV, Type 1 for 450GeV [0]: ');
if isempty(energyflag)
    energyflag = 0;
end

%new version of the BPM model (Impedance model from B. Salvant (2009) corrected
%by C. Zannini to account for the constant term (2014))
BPHdnewdipx=dlmread('BPHs_xdriv_new_re.txt','',2,0);
BPHdnewquadx=dlmread('BPHs_xdet_new_re.txt','',2,0);
BPVdnewdipy=dlmread('BPVs_ydriv_new_re.txt','',2,0);
BPVdnewquady=dlmread('BPVs_ydet_new_re.txt','',2,0);
BPHdnewimdipx=dlmread('BPHs_xdriv_new_im.txt','',2,0);
BPHdnewimquadx=dlmread('BPHs_xdet_new_im.txt','',2,0);
BPVdnewimdipy=dlmread('BPVs_ydriv_new_im.txt','',2,0);
BPVdnewimquady=dlmread('BPVs_ydet_new_im.txt','',2,0);

%BPVdnew=dlmread(strcat('ImpedancesBPVforHDTLbeta',opt,'.dat'),'',0,0);
%old model (top-bottom left-right simmetry B. Salvant (2009))
BPHd=dlmread(strcat('ImpedancesBPHforHDTLbeta',opt,'.dat'),'',0,0);
BPVd=dlmread(strcat('ImpedancesBPVforHDTLbeta',opt,'.dat'),'',0,0);

%enamel flanges
EFd=dlmread(strcat('ImpedancesenamelFlangeforHDTLbeta',opt,'.dat'),'',0,0);

%RF cavities
RF200d=dlmread(strcat('ImpedancesRFcav200forHDTLbeta',opt,'.dat'),'',0,0);
RF800d=dlmread(strcat('ImpedancesRFcav800forHDTLbeta',opt,'.dat'),'',0,0);

%kickers
kickerd=dlmread(strcat('kicker2015_beta',opt,'.dat'),'',0,0);
%Wall (resistive wall+indirect space charge)
walld=dlmread(strcat('wall_beta',opt,'.dat'),'',0,0);
%transitions
transitionsd=dlmread(strcat('transitions_beta',opt,'.txt'),'',0,0);

%Readind data for the different elements
BPHf=real(BPHd(length(BPHd(:,1))/2:end,1));
BPHfnew=BPHdnewquadx(:,1);
%BPHxdip=(BPHd(length(BPHd(:,1))/2:end,2));
BPHxdip=BPHdnewdipx(:,2)+1i*BPHdnewimdipx(:,2);
BPHydip=(BPHd(length(BPHd(:,1))/2:end,3));
%BPHxquad=(BPHd(length(BPHd(:,1))/2:end,4));
BPHxquad=BPHdnewquadx(:,2)+1i*BPHdnewimquadx(:,2);
BPHyquad=(BPHd(length(BPHd(:,1))/2:end,5));
BPHX=BPHxdip+BPHxquad;
BPHY=BPHydip+BPHyquad;
%BPH=[BPHf BPHxdip BPHydip BPHxquad BPHyquad];

BPVf=real(BPVd(length(BPVd(:,1))/2:end,1));
BPVfnew=BPVdnewquady(:,1);
BPVxdip=(BPVd(length(BPVd(:,1))/2:end,2));
%BPVydip=(BPVd(length(BPVd(:,1))/2:end,3));
BPVydip=BPVdnewdipy(:,2)+1i*BPVdnewimdipy(:,2);
BPVxquad=(BPVd(length(BPVd(:,1))/2:end,4));
%BPVyquad=(BPVd(length(BPVd(:,1))/2:end,5));
BPVyquad=BPVdnewquady(:,2)+1i*BPVdnewimquady(:,2);
BPVX=BPVxdip+BPVxquad;
BPVY=BPVydip+BPVyquad;
%BPV=[BPVf BPVxdip BPVydip BPVxquad BPVyquad];

EFf=real(EFd(length(EFd(:,1))/2:end,1));
EFxdip=(EFd(length(EFd(:,1))/2:end,2));
EFydip=(EFd(length(EFd(:,1))/2:end,3));
EFxquad=(EFd(length(EFd(:,1))/2:end,4));
EFyquad=(EFd(length(EFd(:,1))/2:end,5));
EFX=EFxdip+EFxquad;
EFY=EFydip+EFyquad;
EF=[EFf EFxdip EFydip EFxquad EFyquad];

RF200f=real(RF200d(length(RF200d(:,1))/2:end,1));
RF200xdip=(RF200d(length(RF200d(:,1))/2:end,2));
RF200ydip=(RF200d(length(RF200d(:,1))/2:end,3));
RF200xquad=(RF200d(length(RF200d(:,1))/2:end,4));
RF200yquad=(RF200d(length(RF200d(:,1))/2:end,5));
RF200X=RF200xdip+RF200xquad;
RF200Y=RF200ydip+RF200yquad;
RF200=[RF200f RF200xdip RF200ydip RF200xquad RF200yquad];

RF800f=real(RF800d(length(RF800d(:,1))/2:end,1));
RF800xdip=(RF800d(length(RF800d(:,1))/2:end,2));
RF800ydip=(RF800d(length(RF800d(:,1))/2:end,3));
RF800xquad=(RF800d(length(RF800d(:,1))/2:end,4));
RF800yquad=(RF800d(length(RF800d(:,1))/2:end,5));
RF800X=RF800xdip+RF800xquad;
RF800Y=RF800ydip+RF800yquad;
RF800=[RF800f RF800xdip RF800ydip RF800xquad RF800yquad];

kickerf=real(kickerd(:,1));
kickerxdip=(kickerd(:,2));
kickerydip=(kickerd(:,3));
kickerxquad=(kickerd(:,4));
kickeryquad=(kickerd(:,5));
kickerX=kickerxdip+kickerxquad;
kickerY=kickerydip+kickeryquad;
kicker=[kickerf kickerxdip kickerydip kickerxquad kickeryquad];

wallf=real(walld(:,1))/1e9;
wallxdip=(walld(:,2));
wallydip=(walld(:,3));
wallxquad=(walld(:,4));
wallyquad=(walld(:,5));
wallX=wallxdip+wallxquad;
wallY=wallydip+wallyquad;
wall=[wallf wallxdip wallydip wallxquad wallyquad];

transitionsf=real(transitionsd(:,1))/1e9;
fact=[0 1 2; 1 1 0]';
factI=interp1(fact(:,1),fact(:,2),transitionsf);
transitionsxdip=(transitionsd(:,2));
transitionsydip=(transitionsd(:,3)).*factI;
transitionsxquad=(transitionsd(:,4));
transitionsyquad=(transitionsd(:,5));
transitionsX=transitionsxdip+transitionsxquad;
transitionsY=transitionsydip+transitionsyquad;
transitions=[transitionsf transitionsxdip transitionsydip transitionsxquad transitionsyquad];


%interpolation
f=[0:1e-6:1e-3 2e-3:1e-3:1.9];
RF200xdipt=interp1(RF200f,RF200xdip,f);
RF800xdipt=interp1(RF800f,RF800xdip,f);
EFxdipt=interp1(EFf,EFxdip,f);
BPVxdipt=interp1(BPVf,BPVxdip,f);
%BPHxdipt=interp1(BPHf,BPHxdip,f);
BPHxdipt=interp1(BPHfnew,BPHxdip,f);
kickerxdipt=interp1(kickerf,kickerxdip,f);
wallxdipt=interp1(wallf,wallxdip,f);
transitionsxdipt=interp1(transitionsf,transitionsxdip,f);

RF200xquadt=interp1(RF200f,RF200xquad,f);
RF800xquadt=interp1(RF800f,RF800xquad,f);
EFxquadt=interp1(EFf,EFxquad,f);
BPVxquadt=interp1(BPVf,BPVxquad,f);
%BPHxquadt=interp1(BPHf,BPHxquad,f);
BPHxquadt=interp1(BPHfnew,BPHxquad,f);
kickerxquadt=interp1(kickerf,kickerxquad,f);
wallxquadt=interp1(wallf,wallxquad,f);
transitionsxquadt=interp1(transitionsf,transitionsxquad,f);

RF200ydipt=interp1(RF200f,RF200ydip,f);
RF800ydipt=interp1(RF800f,RF800ydip,f);
EFydipt=interp1(EFf,EFydip,f);
%BPVydipt=interp1(BPVf,BPVydip,f);
BPVydipt=interp1(BPVfnew,BPVydip,f);
BPHydipt=interp1(BPHf,BPHydip,f);
kickerydipt=interp1(kickerf,kickerydip,f);
wallydipt=interp1(wallf,wallydip,f);
transitionsydipt=interp1(transitionsf,transitionsydip,f);

RF200yquadt=interp1(RF200f,RF200yquad,f);
RF800yquadt=interp1(RF800f,RF800yquad,f);
EFyquadt=interp1(EFf,EFyquad,f);
%BPVyquadt=interp1(BPVf,BPVyquad,f);
BPVyquadt=interp1(BPVfnew,BPVyquad,f);
BPHyquadt=interp1(BPHf,BPHyquad,f);
kickeryquadt=interp1(kickerf,kickeryquad,f);
wallyquadt=interp1(wallf,wallyquad,f);
transitionsyquadt=interp1(transitionsf,transitionsyquad,f);

%% Building of the impedance model

loadflag = input('Type 0 for the full SPS model, Type 1 for interactive building of the model [0]: ');
if isempty(loadflag)
    loadflag = 0;
end

if loadflag==0
    a1=1; s1= '_EF';
    a2=1; s2='_BPH';
    a3=1; s3='_BPV';
    a4=1; s4='_RF200';
    a5=1; s5='_RF800';
    a6=1; s6='_kicker';
    a7=1; s7='_wall';
    a8=1; s8='_trans';
    a=[a1 a2 a3 a3 a5 a6 a7 a8];
    s=[s1 s2 s3 s4 s5 s6 s7 s8 opt];
end

if loadflag==1
    a1=input('Do you want include enamel flanges (y/n) [y]: ','s');
    if isempty(a1)
    a1 = 'y';
    end
   if strcmp(a1,'y')
       a1=1;
       s1='_EF';
   else a1=0;
       s1='';
   end
   a2=input('Do you want include BPH (y/n) [y]: ','s');
    if isempty(a2)
    a2 = 'y';
    end
   if strcmp(a2,'y')
       a2=1;
       s2='_BPH';
   else a2=0;
       s2='';
   end
   a3=input('Do you want include BPV (y/n) [y]: ','s');
    if isempty(a3)
    a3 = 'y';
    end
   if strcmp(a3,'y')
       a3=1;
       s3='_BPV';
   else a3=0;
       s3='';
   end
   a4=input('Do you want include RF200MHz cavities (y/n) [y]: ','s');
    if isempty(a4)
    a4 = 'y';
    end
   if strcmp(a4,'y')
       a4=1;
       s4='_RF200';
   else a4=0;
       s4='';
   end
   a5=input('Do you want include RF800 MHz cavities (y/n) [y]: ','s');
    if isempty(a5)
    a5 = 'y';
    end
   if strcmp(a5,'y')
       a5=1;
       s5='_RF800';
   else a5=0;
       s5='';
   end
    a6=input('Do you want include kickers (y/n) [y]: ','s');
    if isempty(a6)
    a6 = 'y';
    end
   if strcmp(a6,'y')
       a6=1;
       s6='_kicker';
   else a6=0;
       s6='';
   end
    a7=input('Do you want include wall (y/n) [y]: ','s');
    if isempty(a7)
    a7 = 'y';
    end
   if strcmp(a7,'y')
       a7=1;
       s7='_wall';
   else a7=0;
       s7='';
   end
   a8=input('Do you want include transitions (y/n) [y]: ','s');
    if isempty(a8)
    a8 = 'y';
    end
   if strcmp(a8,'y')
       a8=1;
       s8='_trans';
   else a8=0;
       s8='';
   end
   a=[a1 a2 a3 a4 a5 a6 a7 a8];
   s=[s1 s2 s3 s4 s5 s6 s7 s8 opt];
end

%Somma
ImpXdipf=EFxdipt*a(1)+BPHxdipt*a(2)+BPVxdipt*a(3)+RF200xdipt*a(4)+RF800xdipt*a(5)+kickerxdipt*a(6)+wallxdipt*a(7)+transitionsxdipt*a(8);
ImpYdipf=EFydipt*a(1)+BPHydipt*a(2)+BPVydipt*a(3)+RF200ydipt*a(4)+RF800ydipt*a(5)+kickerydipt*a(6)+wallydipt*a(7)+transitionsydipt*a(8);
ImpXquadf=EFxquadt*a(1)+BPHxquadt*a(2)+BPVxquadt*a(3)+RF200xquadt*a(4)+RF800xquadt*a(5)+kickerxquadt*a(6)+wallxquadt*a(7)+transitionsxquadt*a(8);
ImpYquadf=EFyquadt*a(1)+BPHyquadt*a(2)+BPVyquadt*a(3)+RF200yquadt*a(4)+RF800yquadt*a(5)+kickeryquadt*a(6)+wallyquadt*a(7)+transitionsyquadt*a(8);

ImpdataSPSf=[f' ImpXdipf.' ImpYdipf.' ImpXquadf.' ImpYquadf.'];
save(strcat('ImpmodelSPS',s),'ImpdataSPSf');

ImpdataSPSfHz=[f'*1e9 ImpXdipf.' ImpYdipf.' ImpXquadf.' ImpYquadf.'];
dlmwrite(['ImpmodelSPS',s,'.dat'],ImpdataSPSfHz)

figure(1)
set(gca,'FontSize',20)
plot(real(f),real(ImpXdipf),'r','LineWidth',3);
hold on;
plot(real(f),imag(ImpXdipf),'g','LineWidth',3);
hold on;
plot(real(f),real(ImpXquadf),'r--','LineWidth',3);
hold on;
plot(real(f),imag(ImpXquadf),'g--','LineWidth',3);
hold on;
grid on;
title('Horizontal SPS Impedance model')
xlabel('Frequency [GHz]')
ylabel('Impedance [\Omega/m]')
legend('Real(Driving)','Imag(Driving)','Real(Detuning)','Imag(Detuning)');
saveas(figure(1),strcat('XImpmodelSPS',s),'png')

figure(2)
set(gca,'FontSize',20)
plot(real(f),real(ImpYdipf),'r','LineWidth',3);
hold on;
plot(real(f),imag(ImpYdipf),'g','LineWidth',3);
hold on;
plot(real(f),real(ImpYquadf),'r--','LineWidth',3);
hold on;
plot(real(f),imag(ImpYquadf),'g--','LineWidth',3);
hold on;
grid on;
title('Vertical SPS Impedance model')
xlabel('Frequency [GHz]')
ylabel('Impedance [\Omega/m]')
legend('Real(Driving)','Imag(Driving)','Real(Detuning)','Imag(Detuning)');
saveas(figure(2),strcat('YImpmodelSPS',s),'png')
