from __future__ import division, print_function

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy.interpolate import interp1d
from scipy.constants import m_p, c, e, epsilon_0

import numpy.core.defchararray as np_f

# Fix Python 2.x.
try: input = raw_input
except NameError: pass


def interpolarrayimpdata(dataarray, fvecintp):
    data_old = ['Driving_X [Ohm/m]', 'Driving_Y [Ohm/m]', 'Detuning_X [Ohm/m]', 'Detuning_Y [Ohm/m]']
    #timevec = np.array(np.append(dataarray[0], [dataarray[0][-1]+1, tvecintp[-1]+1]), dtype='float64')
    freqvec = np.array(np.real(dataarray[0]))
    
    df_int =  pd.DataFrame({}, index = fvecintp)
    df_int.index.name = 'Frequency [GHz]'

    for cnt in np.arange(len(data_old)):
        #datatoint = np.array(np.append(dataarray[cnt+1], [0, 0]), dtype='float64')
        df_int[data_old[cnt]] = interp1d(freqvec, dataarray[cnt+1])(fvecintp)
        
    return df_int


versionflag = None
print('Choose the version of the SPS wake model:')
while versionflag not in ["0", "1", "2"]:
    versionflag = input('Type 0 for the 2015 version (legacy), \ntype 1 for the 2018 version, \ntype 2 for the PostLS2 version: \n');

    if versionflag == '0':
        version = '_2015'
    elif versionflag == '1':
        version = '_2018'
    elif versionflag == '2':
        version = '_PostLS2'
    else:
        print('Invalid input')


opticflag = None
print('Choose the optics:')
while opticflag not in ["0", "1", "2"]:
    opticflag = input('Type 0 for Q20, \ntype 2 for Q26: \n'); #\ntype 1 for Q22, 

    if opticflag == '0':
        opt = '_lowGT'; optprint = 'Q20'
#    elif opticflag == '1':
#        opt = '_Q22'; optprint = 'Q22'
    elif opticflag == '2':
        opt = ''; optprint = 'Q26'
    else:
        print('Invalid input')


# Beam Position Monitors
# old model (top-bottom left-right simmetry B. Salvant (2009))
BPHd = np.genfromtxt('ImpData/ImpedancesBPHforHDTLbeta'+opt+'.dat', delimiter='', dtype=str)
BPHd = np_f.replace(BPHd, 'i', 'j')
BPHd = np.complex_(BPHd)
BPVd = np.genfromtxt('ImpData/ImpedancesBPVforHDTLbeta'+opt+'.dat', delimiter='', dtype=str)
BPVd = np_f.replace(BPVd, 'i', 'j')
BPVd = np.complex_(BPVd)
# new version of the BPM model (Impedance model from B. Salvant (2009) corrected
# by C. Zannini to account for the constant term (2014))
BPHdnewredipx = np.genfromtxt('ImpData/BPHs_xdriv_new_re.txt', dtype='f16', delimiter='', skip_header=2)
BPHdnewrequadx = np.genfromtxt('ImpData/BPHs_xdet_new_re.txt', dtype='f16', delimiter='', skip_header=2)
BPVdnewredipy = np.genfromtxt('ImpData/BPVs_ydriv_new_re.txt', dtype='f16', delimiter='', skip_header=2)
BPVdnewrequady = np.genfromtxt('ImpData/BPVs_ydet_new_re.txt', dtype='f16', delimiter='', skip_header=2)
BPHdnewimdipx = np.genfromtxt('ImpData/BPHs_xdriv_new_im.txt', dtype='f16', delimiter='', skip_header=2)
BPHdnewimquadx = np.genfromtxt('ImpData/BPHs_xdet_new_im.txt', dtype='f16', delimiter='', skip_header=2)
BPVdnewimdipy = np.genfromtxt('ImpData/BPVs_ydriv_new_im.txt', dtype='f16', delimiter='', skip_header=2)
BPVdnewimquady = np.genfromtxt('ImpData/BPVs_ydet_new_im.txt', dtype='f16', delimiter='', skip_header=2)

# Enamel Flanges
EFd = np.genfromtxt('ImpData/ImpedancesenamelFlangeforHDTLbeta'+opt+'.dat', delimiter='', dtype=str)
EFd = np_f.replace(EFd, 'i', 'j')
EFd = np.complex_(EFd)

# RF cavities
RF200d = np.genfromtxt('ImpData/ImpedancesRFcav200forHDTLbeta'+opt+'.dat', delimiter='', dtype=str)
RF200d = np_f.replace(RF200d, 'i', 'j')
RF200d = np.complex_(RF200d)
RF800d = np.genfromtxt('ImpData/ImpedancesRFcav800forHDTLbeta'+opt+'.dat', delimiter='', dtype=str)
RF800d = np_f.replace(RF800d, 'i', 'j')
RF800d = np.complex_(RF800d)

# Wall (resistive wall and indirect space charge)
walld = np.genfromtxt('ImpData/wall_beta'+opt+'.dat', delimiter='', dtype=str)
walld = np_f.replace(walld, 'i', 'j')
walld = np.complex_(walld)

# Transitions
transitionsd = np.genfromtxt('ImpData/transitions_beta'+opt+'.txt', delimiter='', dtype=str)
transitionsd = np_f.replace(transitionsd, 'i', 'j')
transitionsd = np.complex_(transitionsd)

# Kickers, electrostatic septum (ZS), EF
if version == '_PostLS2':
    kickerd = np.genfromtxt('ImpData/kicker2018v2_beta'+opt+'.dat', delimiter='', dtype=str)
    kickerd = np_f.replace(kickerd, 'i', 'j')
    kickerd = np.complex_(kickerd)
    ZSd = np.genfromtxt('ImpData/ZS_'+optprint+version+'_IMP.txt', delimiter='', dtype=str)
    ZSd = np_f.replace(ZSd, '+-', '-')
    ZSd = np.complex_(ZSd)
    EFd.T[1:] = 0    # EF shielded during LS2
elif version == '_2018':
    kickerd = np.genfromtxt('ImpData/kicker2018v2_beta'+opt+'.dat', delimiter='', dtype=str)
    kickerd = np_f.replace(kickerd, 'i', 'j')
    kickerd = np.complex_(kickerd)
    ZSd = np.genfromtxt('ImpData/ZS_'+optprint+'_PreLS2_IMP.txt', delimiter='', dtype=str)
    ZSd = np_f.replace(ZSd, '+-', '-')
    ZSd = np.complex_(ZSd)
else:
    kickerd = np.genfromtxt('ImpData/kicker2015_beta'+opt+'.dat', delimiter='', dtype=str)
    kickerd = np_f.replace(kickerd, 'i', 'j')
    kickerd = np.complex_(kickerd)
    ZSd = np.genfromtxt('ImpData/ZS_'+optprint+'_PreLS2_IMP.txt', delimiter='', dtype=str)
    ZSd = np_f.replace(ZSd, '+-', '-')
    ZSd = np.complex_(ZSd)
    ZSd.T[1:] = 0    # No ZS contribution in legacy model

# Organizing data for the different elements

BPHf = np.real(BPHd.T[0][len(BPHd.T[0])//2::])
BPHfnew = BPHdnewrequadx.T[0]
#BPHxdip = BPHd.T[1][len(BPHd.T[0])//2::]
BPHxdip = BPHdnewredipx.T[1] + 1j * BPHdnewimdipx.T[1]
BPHydip = BPHd.T[2][len(BPHd.T[0])//2::]
#BPHxquad = BPHd.T[3][len(BPHd.T[0])//2::]
BPHxquad = BPHdnewrequadx.T[1] + 1j * BPHdnewimquadx.T[1]
BPHyquad = BPHd.T[4][len(BPHd.T[0])//2::]
BPH = np.array([BPHfnew, BPHxdip, interp1d(BPHf, BPHydip)(BPHfnew), BPHxquad, interp1d(BPHf, BPHyquad)(BPHfnew)])

BPVf = np.real(BPVd.T[0][len(BPVd.T[0])//2::])
BPVfnew = BPVdnewrequady.T[0]
BPVxdip = BPVd.T[1][len(BPVd.T[0])//2::]
#BPVydip = BPVd.T[2][len(BPVd.T[0])//2::]
BPVydip = BPVdnewredipy.T[1] + 1j * BPVdnewimdipy.T[1]
BPVxquad = BPVd.T[3][len(BPVd.T[0])//2::]
#BPVyquad = BPVd.T[4][len(BPVd.T[0])//2::]
BPVyquad = BPVdnewrequady.T[1] + 1j * BPVdnewimquady.T[1]
BPV = np.array([BPVf, BPVxdip, interp1d(BPVfnew, BPVydip)(BPVf), BPVxquad, interp1d(BPVfnew, BPVyquad)(BPVf)])

EFf = np.real(EFd.T[0][len(EFd.T[0])//2::])
EFxdip = EFd.T[1][len(EFd.T[0])//2::]
EFydip = EFd.T[2][len(EFd.T[0])//2::]
EFxquad = EFd.T[3][len(EFd.T[0])//2::]
EFyquad = EFd.T[4][len(EFd.T[0])//2::]
EF = np.array([EFf, EFxdip, EFydip, EFxquad, EFyquad])

RF200f = np.real(RF200d.T[0][len(RF200d.T[0])//2::])
RF200xdip = RF200d.T[1][len(RF200d.T[0])//2::]
RF200ydip = RF200d.T[2][len(RF200d.T[0])//2::]
RF200xquad = RF200d.T[3][len(RF200d.T[0])//2::]
RF200yquad = RF200d.T[4][len(RF200d.T[0])//2::]
RF200 = np.array([RF200f, RF200xdip, RF200ydip, RF200xquad, RF200yquad])

RF800f = np.real(RF800d.T[0][len(RF800d.T[0])//2::])
RF800xdip = RF800d.T[1][len(RF800d.T[0])//2::]
RF800ydip = RF800d.T[2][len(RF800d.T[0])//2::]
RF800xquad = RF800d.T[3][len(RF800d.T[0])//2::]
RF800yquad = RF800d.T[4][len(RF800d.T[0])//2::]
RF800 = np.array([RF800f, RF800xdip, RF800ydip, RF800xquad, RF800yquad])

wallf = np.real(walld.T[0])/1e9
wallxdip = walld.T[1]
wallydip = walld.T[2]
wallxquad = walld.T[3]
wallyquad = walld.T[4]
wall = np.array([wallf, wallxdip, wallydip, wallxquad, wallyquad])

transitionsf = np.real(transitionsd.T[0])/1e9
fact = np.array([(0, 1, 2), (1, 1, 0)])
factint = interp1d(fact[0],fact[1])(transitionsf)
transitionsxdip = transitionsd.T[1]
transitionsydip = transitionsd.T[2] * factint
transitionsxquad = transitionsd.T[3]
transitionsyquad = transitionsd.T[4]
transitions = np.array([transitionsf, transitionsxdip, transitionsydip, transitionsxquad, transitionsyquad])


# Interpolate data

# fvec in GHz
fvecintp = np.append(np.arange(0,1e-3,1e-6), np.arange(2e-3,1.901,1e-3)) # in [GHz]
#fvecintp1 = np.arange(0,1.9000001,1e-6) # in [GHz] small steps
#fvecintp2 = np.arange(0,1.9000001,1e-3) # in [GHz] big steps

BPHd_int_df = interpolarrayimpdata(BPH, fvecintp)
BPVd_int_df = interpolarrayimpdata(BPV, fvecintp)

EFd_int_df = interpolarrayimpdata(EF, fvecintp)

RF200d_int_df = interpolarrayimpdata(RF200, fvecintp)
RF800d_int_df = interpolarrayimpdata(RF800, fvecintp)

kickerd_int_df = interpolarrayimpdata(kickerd.T, fvecintp)

transitionsd_int_df = interpolarrayimpdata(transitions, fvecintp)

walld_int_df = interpolarrayimpdata(wall, fvecintp)

ZSd_int_df = interpolarrayimpdata(ZSd.T, fvecintp)


# Building the impedance model

components = ['Enameled Flanges (EF)', 'Horizontal Beam Position monitors (BPH)', 'Vertical Beam Position monitors (BPV)',
             'RF200 cavities', 'RF800 cavities', 'Kickers', 'Wall impedance', 'Step transitions', 'Electrostatic septum (ZS)']
components_print = ['_EF', '_BPH', '_BPV', '_RF200', '_RF800', '_kicker', '_wall', '_steps', '_ZS']

loadflag = None
while loadflag not in ["0", "1"]:
    loadflag = input('Type 0 for the full SPS model, \ntype 1 for interactive building of the model: \n');

    if loadflag == '0':
        a = np.ones(len(components))
        namestr = 'Complete_imp_model'
    elif loadflag == '1':
        a = np.zeros(len(components))
        fns = []
        if version == '_PostLS2':
            a[ccomp] = 0
            fns.append('')
            components = components[1:]
            components_print = components_print[1:]
        for ccomp, comp in enumerate(components):
            if version == '_2015' and comp == 'Electrostatic septum (ZS)':
                a[ccomp] = 0
                fns.append('')
                continue
            inp = input('Do you want include the '+comp+' (y/n) [y]: ')
            if inp != 'n':
                a[ccomp] = 1
                fns.append(components_print[ccomp])
                #print(comp+' included')
            else:
                fns.append('')
                print(comp+' NOT included')
        namestr = ('Imp_model_with{:s}{:s}{:s}{:s}{:s}{:s}{:s}{:s}{:s}'.format(*fns))
    else:
        print('Invalid input')


df_completeimp =  pd.DataFrame({}, index = fvecintp)
df_completeimp.index.name = 'Frequency [GHz]'

data = ['Driving_X [Ohm/m]', 'Driving_Y [Ohm/m]', 'Detuning_X [Ohm/m]', 'Detuning_Y [Ohm/m]']

for cnt, datatype in enumerate(data):
    df_completeimp[datatype] = (EFd_int_df[datatype] * a[0] + BPHd_int_df[datatype] * a[1] + BPVd_int_df[datatype] * a[2]
                                 + RF200d_int_df[datatype] * a[3] + RF800d_int_df[datatype] * a[4]
                                 + kickerd_int_df[datatype] * a[5] + walld_int_df[datatype] * a[6]
                                 + transitionsd_int_df[datatype] * a[7] + ZSd_int_df[datatype] * a[8])

#df_completeimp.to_hdf('SPS_'+namestr+version+'_'+optprint+'.h5','df') # Save impedance model as DataFrame
#df_completeimp.head()

# Save data in a .txt file:
Datatype = ('Frequency [GHz]\t \tDipolX [Ohm/m]\t  \tDipolY [Ohm/m]\t \tQuadX [Ohm/m]\t \tQuadY [Ohm/m]')

Datawrite = np.array([df_completeimp.index, 
                      df_completeimp['Driving_X [Ohm/m]'], df_completeimp['Driving_Y [Ohm/m]'],
                      df_completeimp['Detuning_X [Ohm/m]'], df_completeimp['Detuning_Y [Ohm/m]']])

np.savetxt('SPS_'+namestr+version+'_'+optprint+'.txt', Datawrite.T, delimiter='\t', header=Datatype,  fmt='%.14f')  
print('File \'SPS_'+namestr+version+'_'+optprint+'.txt\' has been created.\n')


# Plot

fig1, ax1 = plt.subplots(2, 2, figsize=(15,12), num=12);
fig1.suptitle('SPS_'+namestr+version+'_'+optprint, fontsize=18)
                                                                                                                                                                               
ax1[0][0].plot(df_completeimp.index, np.real(df_completeimp['Driving_X [Ohm/m]']), label = 'Re{$Z_{x}^{driv}$}');
ax1[0][0].plot(df_completeimp.index, np.imag(df_completeimp['Driving_X [Ohm/m]']), '--', label = 'Im{$Z_{x}^{driv}$}');
ax1[0][0].grid()
ax1[0][0].set_title('Horizontal dipolar part', fontsize=14)
ax1[0][0].set_ylabel('Impedance [$\Omega$/m]', fontsize=12);
ax1[0][0].set_xlabel('Frequency [GHz]', fontsize=12);
ax1[0][0].legend(fontsize=12);

ax1[0][1].plot(df_completeimp.index, np.real(df_completeimp['Driving_Y [Ohm/m]']), label = 'Re{$Z_{y}^{driv}$}');
ax1[0][1].plot(df_completeimp.index, np.imag(df_completeimp['Driving_Y [Ohm/m]']),'--', label = 'Im{$Z_{y}^{driv}$}');
ax1[0][1].grid()
ax1[0][1].set_title('Vertical dipolar part', fontsize=14)
ax1[0][1].set_ylabel('Impedance [$\Omega$/m]', fontsize=12);
ax1[0][1].set_xlabel('Frequency [GHz]', fontsize=12);
ax1[0][1].legend(fontsize=12);

ax1[1][0].plot(df_completeimp.index, np.real(df_completeimp['Detuning_X [Ohm/m]']), label = 'Re{$Z_{x}^{det}$}');
ax1[1][0].plot(df_completeimp.index, np.imag(df_completeimp['Detuning_X [Ohm/m]']), '--', label = 'Im{$Z_{x}^{det}$}');
ax1[1][0].grid()
ax1[1][0].set_title('Horizontal quadrupolar part', fontsize=14)
ax1[1][0].set_ylabel('Impedance [$\Omega$/m]', fontsize=12);
ax1[1][0].set_xlabel('Frequency [GHz]', fontsize=12);
ax1[1][0].legend(fontsize=12);

ax1[1][1].plot(df_completeimp.index, np.real(df_completeimp['Detuning_Y [Ohm/m]']), label = 'Re{$Z_{y}^{det}$}');
ax1[1][1].plot(df_completeimp.index, np.imag(df_completeimp['Detuning_Y [Ohm/m]']), '--', label = 'Im{$Z_{y}^{det}$}');
ax1[1][1].grid()
ax1[1][1].set_title('Vertical quadrupolar part', fontsize=14)
ax1[1][1].set_ylabel('Impedance [$\Omega$/m]', fontsize=12);
ax1[1][1].set_xlabel('Frequency [GHz]', fontsize=12);
ax1[1][1].legend(fontsize=12);

plt.show()
#fig1.savefig('SPS_'+namestr+version+'_'+optprint+'.png', bbox_inches='tight')
