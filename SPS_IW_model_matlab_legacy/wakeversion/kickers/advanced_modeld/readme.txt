2015 kicker model. All MKEs are serigraphed. Update of the MKPS (MKPA and MKPC) wake model
Use the matlab script to build up the more updated SPS kicker impedance model.

1	MKQH
2	MKQV
3	MKDV1
4	MKDV2
5	MKDH1
6	MKDH2
7	MKDH3
8	MKPA
9	MKPA
10	MKPC
11	MKPL
12	MKELser
13	MKELser
14	MKESser
15	MKESser
16	MKELser
17	MKELser
18	MKELser
19	MKESser
