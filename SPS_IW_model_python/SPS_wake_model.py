from __future__ import division, print_function

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy.interpolate import interp1d
from scipy.constants import m_p, c, e, epsilon_0

# Fix Python 2.x.
try: input = raw_input
except NameError: pass


def interpoldfdata(df, tvecintp):
    data = ['Driving_X [V/mm/pC]', 'Detuning_X [V/mm/pC]', 'Driving_Y [V/mm/pC]', 'Detuning_Y [V/mm/pC]']
    
    df_int =  pd.DataFrame({}, index = tvecintp)
    df_int.index.name = 'Time [ns]'
    
    for dfdata in data:
        df_int[dfdata] = interp1d(df.index.values, df[dfdata].values)(tvecintp)
    
    return df_int

def interpolarraydata(dataarray, tvecintp):
    data_old = ['Driving_X [V/mm/pC]', 'Driving_Y [V/mm/pC]', 'Detuning_X [V/mm/pC]', 'Detuning_Y [V/mm/pC]']
    timevec = np.array(np.append(dataarray.T[0], [dataarray.T[0][-1]+1, tvecintp[-1]+1]), dtype='float64')
    
    df_int =  pd.DataFrame({}, index = tvecintp)
    df_int.index.name = 'Time [ns]'

    for cnt in np.arange(len(data_old)):
        datatoint = np.array(np.append(dataarray.T[cnt+1], [0, 0]), dtype='float64')
        df_int[data_old[cnt]] = interp1d(timevec, datatoint)(tvecintp)
        
    return df_int


versionflag = None
print('Choose the version of the SPS wake model:')
while versionflag not in ["0", "1", "2"]:
    versionflag = input('Type 0 for the 2015 version (legacy), \ntype 1 for the 2018 version, \ntype 2 for the PostLS2 version: \n');

    if versionflag == '0':
        version = '_2015'
    elif versionflag == '1':
        version = '_2018'
    elif versionflag == '2':
        version = '_PostLS2'
    else:
        print('Invalid input')


opticflag = None
print('Choose the optics:')
while opticflag not in ["0", "1", "2"]:
    opticflag = input('Type 0 for Q20, \ntype 1 for Q22, \ntype 2 for Q26: \n');

    if opticflag == '0':
        opt = '_lowGT'; optprint = 'Q20'
    elif opticflag == '1':
        opt = '_Q22'; optprint = 'Q22'
    elif opticflag == '2':
        opt = ''; optprint = 'Q26'
    else:
        print('Invalid input')


BPHd = np.genfromtxt('WakeData/WakesBPHforHDTLnewbeta'+opt+'.dat', dtype='f16', delimiter='')
BPVd = np.genfromtxt('WakeData/WakesBPVforHDTLnewbeta'+opt+'.dat', dtype='f16', delimiter='')
EFd = np.genfromtxt('WakeData/WakesenamelFlangeforHDTLbeta'+opt+'.dat', dtype='f16', delimiter='')
RF200d = np.genfromtxt('WakeData/WakesRFcav200forHDTLbeta'+opt+'.dat', dtype='f16', delimiter='')
RF800d = np.genfromtxt('WakeData/WakesRFcav800forHDTLbeta'+opt+'.dat', dtype='f16', delimiter='')
walld = np.genfromtxt('WakeData/wall_beta'+opt+'.txt', dtype='f16', delimiter='')
transitionsd = np.genfromtxt('WakeData/transitionsnew_beta'+opt+'.txt', dtype='f16', delimiter='')

kickerd = np.genfromtxt('WakeData/newkickers_'+optprint+version+'.txt', dtype='f16', delimiter='')  # NEW kicker model 

if version == '_PostLS2':
    ZSd = np.genfromtxt('WakeData/ZS_'+optprint+version+'.txt', dtype='f16', delimiter='')
    EFd.T[1:] = 0    # EF shielded during LS2
elif version == '_2018':
    ZSd = np.genfromtxt('WakeData/ZS_'+optprint+'_PreLS2.txt', dtype='f16', delimiter='')
else:
    ZSd = np.genfromtxt('WakeData/ZS_'+optprint+'_PreLS2.txt', dtype='f16', delimiter='')
    ZSd.T[1:] = 0    # No ZS contribution in legacy model
    # Legacy kicker; comment line below to use new kicker model
    kickerd = np.genfromtxt('WakeData/kickers'+optprint+'_2015.txt', dtype='f16', delimiter='')

walld_from = np.zeros(np.shape(np.array(walld).T)) # Rearange as different format than others
walld_from[0] = walld.T[0] * 1e9
walld_from[1] = walld.T[1] / 1000.0
walld_from[2] = walld.T[3] / 1000.0                # Different order of components in file
walld_from[3] = walld.T[2] / 1000.0
walld_from[4] = walld.T[4] / 1000.0

tvecintp = np.arange(1e-2, 10.01, 1e-2)            # Time vector for single bunch simulations (in [ns])
#tvecintp = walld_from[0][1:];  optprint += '_LW'   # Time vector for multi bunch simulation (in [ns]) DEVELOP

BPHd_int_df = interpolarraydata(BPHd, tvecintp)
BPVd_int_df = interpolarraydata(BPVd, tvecintp)

EFd_int_df = interpolarraydata(EFd, tvecintp)

RF200d_int_df = interpolarraydata(RF200d, tvecintp)
RF800d_int_df = interpolarraydata(RF800d, tvecintp)

kickerd_int_df = interpolarraydata(kickerd, tvecintp)

transitionsd_int_df = interpolarraydata(transitionsd, tvecintp)

walld_int_df = interpolarraydata(walld_from.T, tvecintp)

ZSd_int_df = interpolarraydata(ZSd.T, tvecintp)


components = ['Enameled Flanges (EF)', 'Horizontal Beam Position monitors (BPH)', 'Vertical Beam Position monitors (BPV)',
             'RF200 cavities', 'RF800 cavities', 'Kickers', 'Wall impedance', 'Step transitions', 'Electrostatic septum (ZS)']
components_print = ['_EF', '_BPH', '_BPV', '_RF200', '_RF800', '_kicker', '_wall', '_steps', '_ZS']

loadflag = None
while loadflag not in ["0", "1"]:
    loadflag = input('Type 0 for the full SPS model, \ntype 1 for interactive building of the model: \n');

    if loadflag == '0':
        a = np.ones(len(components))
        namestr = 'complete_wake_model'
    elif loadflag == '1':
        a = np.zeros(len(components))
        fns = []
        if version == '_PostLS2':
            a[ccomp] = 0
            fns.append('')
            components = components[1:]
            components_print = components_print[1:]
        for ccomp, comp in enumerate(components):
            if version == '_2015' and comp == 'Electrostatic septum (ZS)':
                a[ccomp] = 0
                fns.append('')
                continue
            inp = input('Do you want include the '+comp+' (y/n) [y]: ')
            if inp != 'n':
                a[ccomp] = 1
                fns.append(components_print[ccomp])
                #print(comp+' included')
            else:
                fns.append('')
                print(comp+' NOT included')
        namestr = ('wake_model_with{:s}{:s}{:s}{:s}{:s}{:s}{:s}{:s}{:s}'.format(*fns))
    else:
        print('Invalid input')


df_completewake =  pd.DataFrame({}, index = tvecintp)
df_completewake.index.name = 'Time [ns]'

df_completewakezero =  pd.DataFrame({'Driving_X [V/mm/pC]': 0.0,
                                     'Driving_Y [V/mm/pC]': 0.0,
                                     'Detuning_X [V/mm/pC]': 0.0,
                                     'Detuning_Y [V/mm/pC]': 0.0}, index = [0.0])
df_completewakezero.index.name = 'Time [ns]'

data = ['Driving_X [V/mm/pC]', 'Driving_Y [V/mm/pC]', 'Detuning_X [V/mm/pC]', 'Detuning_Y [V/mm/pC]']

for cnt, datatype in enumerate(data):
    df_completewake[datatype] = (EFd_int_df[datatype] * a[0] + BPHd_int_df[datatype] * a[1] + BPVd_int_df[datatype] * a[2]
                                 + RF200d_int_df[datatype] * a[3] + RF800d_int_df[datatype] * a[4]
                                 + kickerd_int_df[datatype] * a[5] + walld_int_df[datatype] * a[6]
                                 + transitionsd_int_df[datatype] * a[7] + ZSd_int_df[datatype] * a[8])

df_completewake=df_completewake.append(df_completewakezero) # Add zeros for PyHT
df_completewake.sort_index(inplace=True)
#df_completewake.to_hdf('SPS_'+namestr+version+'_'+optprint+'.h5','df') # Save impedance model as DataFrame
#df_completewake.head()


# Save data in a .txt file directly usable for  PyHEADTAIL:
Datatype = ('Waketime [ns]\t \tDipolX [V/pC/mm]\t  \tDipolY [V/pC/mm]\t \tQuadX [V/pC/mm]\t \tQuadY [V/pC/mm]')

Datawrite = np.array([df_completewake.index, 
                      df_completewake['Driving_X [V/mm/pC]'], df_completewake['Driving_Y [V/mm/pC]'],
                      df_completewake['Detuning_X [V/mm/pC]'], df_completewake['Detuning_Y [V/mm/pC]']])

np.savetxt('SPS_'+namestr+version+'_'+optprint+'.txt', Datawrite.T, delimiter='\t', header=Datatype,  fmt='%.14f')  
print('File \'SPS_'+namestr+version+'_'+optprint+'.txt\' has been created.\n')


fig1, ax1 = plt.subplots(2, 2, figsize=(15,10), num=12);
fig1.suptitle('SPS_'+namestr+version+'_'+optprint, fontsize=18)
                                                                                                                                                                               
ax1[0][0].plot(df_completewake.index, df_completewake['Driving_X [V/mm/pC]']);
ax1[0][0].grid()
ax1[0][0].set_title('Horizontal dipolar part', fontsize=14)
ax1[0][0].set_ylabel('Wake [V/mm/pC]', fontsize=12);
ax1[0][0].set_xlabel('Time [ns]', fontsize=12);
ax1[0][0].legend(['$W^{\mathrm{dip}}_{x}$'], fontsize=12);

ax1[0][1].plot(df_completewake.index, df_completewake['Driving_Y [V/mm/pC]'], 'g');
ax1[0][1].grid()
ax1[0][1].set_title('Vertical dipolar part', fontsize=14)
ax1[0][1].set_ylabel('Wake [V/mm/pC]', fontsize=12);
ax1[0][1].set_xlabel('Time [ns]', fontsize=12);
ax1[0][1].legend(['$W^{\mathrm{dip}}_{y}$'], fontsize=12);

ax1[1][0].plot(df_completewake.index, df_completewake['Detuning_X [V/mm/pC]'],'--');
ax1[1][0].grid()
ax1[1][0].set_title('Horizontal quadrupolar part', fontsize=14)
ax1[1][0].set_ylabel('Wake [V/mm/pC]', fontsize=12);
ax1[1][0].set_xlabel('Time [ns]', fontsize=12);
ax1[1][0].legend(['$W^{\mathrm{quad}}_{x}$'], fontsize=12);

ax1[1][1].plot(df_completewake.index, df_completewake['Detuning_Y [V/mm/pC]'], 'g--');
ax1[1][1].grid()
ax1[1][1].set_title('Vertical quadrupolar part', fontsize=14)
ax1[1][1].set_ylabel('Wake [V/mm/pC]', fontsize=12);
ax1[1][1].set_xlabel('Time [ns]', fontsize=12);
ax1[1][1].legend(['$W^{\mathrm{quad}}_{y}$'], fontsize=12);

plt.show()
#fig1.savefig('SPS_'+namestr+version+'_'+optprint+'.png', bbox_inches='tight')
