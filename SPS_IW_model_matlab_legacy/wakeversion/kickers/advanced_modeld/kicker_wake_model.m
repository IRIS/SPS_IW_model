clear all
close all

MKESser=dlmread('MKESser.txt','',2,0);
MKEL=dlmread('MKEL.txt','',2,0);
MKES=dlmread('MKES.txt','',2,0);
MKELser=dlmread('MKELser.txt','',2,0);
MKPS1=dlmread('MKPA.txt','',2,0);
MKPS2=dlmread('MKPC.txt','',2,0);
MKPL=dlmread('MKPL.txt','',2,0);
MKQH=dlmread('MKQH.txt','',2,0);
MKQV=dlmread('MKQV.txt','',2,0);
MKDH12=dlmread('MKDH12.txt','',2,0);
MKDH3=dlmread('MKDH3.txt','',2,0);
MKDV1=dlmread('MKDV1.txt','',2,0);
MKDV2=dlmread('MKDV2.txt','',2,0);

betaMKQ22=dlmread('betaMKQ22.txt','');

opticflag = input('Type 0 for Q20, Type 1 for Q26 [0] or 2 for Q22: ');
if isempty(opticflag)
    opticflag = 0;
end
    if opticflag==0
    opt='Q20';
    elseif opticflag==1
    opt='Q26';
    elseif opticflag==2
    opt='Q22';
else opt='';
    
end

betaxQ20=[72.28 45.41 37.4 42.85 47.14 51.16 55.5 38.09 44.43 48.36 56.44 94.57 86.75 79.38 72.47 66.02 95.45 80.05 87.52];
betayQ20=[48.36 76.71 91.6 81.1 74.31 68.83 63.65 89.96 78.25 72.33 62.36 35.87 39.48 43.55 48.07 53.05 35.73 43.45 39.36];
betaxQ22=betaMKQ22(:,1)';
betayQ22=betaMKQ22(:,2)';
betaxQ26=[64.52 33.9 25.7 31.2 35.78 40.19 45.04 26.33 32.83 37.07 46.07 91.97 82.33 73.34 65.00 57.33 92.08 73.42 82.42];
betayQ26=[37.19 70 88.3 75.4 67.28 60.75 54.67 85.78 71.59 64.52 52.83 24.04 27.58 31.77 36.63 42.14 24.12 31.95 27.70];
betaavgQ20=55.1;
betaavgQ22=59.0;
betaavgQ26=42.1;
wakexdip=[MKQH(1:2019,2) MKQV(1:2019,2) MKDV1(1:2019,2) MKDV2(1:2019,2) MKDH12(1:2019,2) MKDH12(1:2019,2) MKDH3(1:2019,2) MKPS1(1:2019,2) MKPS1(1:2019,2) MKPS2(1:2019,2) MKPL(1:2019,2) MKELser(1:2019,2) MKELser(1:2019,2) MKESser(1:2019,2) MKESser(1:2019,2) MKELser(1:2019,2) MKELser(1:2019,2) MKELser(1:2019,2) MKESser(1:2019,2)]*diag(eval(strcat('betax',opt)))/eval(strcat('betaavg',opt));
wakeydip=[MKQH(1:2019,3) MKQV(1:2019,3) MKDV1(1:2019,3) MKDV2(1:2019,3) MKDH12(1:2019,3) MKDH12(1:2019,3) MKDH3(1:2019,3) MKPS1(1:2019,3) MKPS1(1:2019,3) MKPS2(1:2019,3) MKPL(1:2019,3) MKELser(1:2019,3) MKELser(1:2019,3) MKESser(1:2019,3) MKESser(1:2019,3) MKELser(1:2019,3) MKELser(1:2019,3) MKELser(1:2019,3) MKESser(1:2019,3)]*diag(eval(strcat('betay',opt)))/eval(strcat('betaavg',opt));
wakexquad=[MKQH(1:2019,4) MKQV(1:2019,4) MKDV1(1:2019,4) MKDV2(1:2019,4) MKDH12(1:2019,4) MKDH12(1:2019,4) MKDH3(1:2019,4) MKPS1(1:2019,4) MKPS1(1:2019,4) MKPS2(1:2019,4) MKPL(1:2019,4) MKELser(1:2019,4) MKELser(1:2019,4) MKESser(1:2019,4) MKESser(1:2019,4) MKELser(1:2019,4) MKELser(1:2019,4) MKELser(1:2019,4) MKESser(1:2019,4)]*diag(eval(strcat('betax',opt)))/eval(strcat('betaavg',opt));
wakeyquad=[MKQH(1:2019,5) MKQV(1:2019,5) MKDV1(1:2019,5) MKDV2(1:2019,5) MKDH12(1:2019,5) MKDH12(1:2019,5) MKDH3(1:2019,5) MKPS1(1:2019,5) MKPS1(1:2019,5) MKPS2(1:2019,5) MKPL(1:2019,5) MKELser(1:2019,5) MKELser(1:2019,5) MKESser(1:2019,5) MKESser(1:2019,5) MKELser(1:2019,5) MKELser(1:2019,5) MKELser(1:2019,5) MKESser(1:2019,5)]*diag(eval(strcat('betay',opt)))/eval(strcat('betaavg',opt));

Xdiptot=sum(wakexdip,2);
Ydiptot=sum(wakeydip,2);
Xquadtot=sum(wakexquad,2);
Yquadtot=sum(wakeyquad,2);

wakedataexp=[MKELser(1:2019,1) Xdiptot Ydiptot Xquadtot Yquadtot];
